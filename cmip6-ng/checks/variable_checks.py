#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-04-08 15:36:01 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Ruth Lorenz || ruth.lorenz@env.ethz.ch
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Variable dependent sanity checks. Mainly checking the full valid
range as well as the valid range of global (not-area weighted!) mean.

WARNING
-------
Please NEVER EVER do any assignments to the 'ds' object here!!
(The checks functions should never change the dataset in any way)

TODO
----
- Several variables miss sensible ranges to check
"""
import logging

from configs.variable_ranges import ERROR_RANGE, WARNING_RANGE

logger = logging.getLogger(__name__)

DIMENSIONS_1D = ('time')
DIMENSIONS_2D = ('lat', 'lon')
DIMENSIONS_3D = ('time', 'lat', 'lon')
DIMENSIONS_3D_2 = ('time', 'y', 'x')
DIMENSIONS_3D_3 = ('time', 'i') # ICON
DIMENSIONS_4D = ('time', 'depth', 'lat', 'lon')
DIMENSIONS_4D_2 = ('time', 'plev', 'lat', 'lon')
DIMENSIONS_4D_3 = ('time', 'depth', 'i') # ICON
DIMENSIONS_4D_4 = ('time', 'plev', 'i') # ICON


def check_range_full(da, varn, warning=False):
    min_ = float(da.min().data)  # force value from dask
    max_ = float(da.max().data)
    unit = da.attrs['units']  # for logging only
    if warning:
        min_ref = WARNING_RANGE[varn][0]
        max_ref = WARNING_RANGE[varn][1]
        if min_ < min_ref or max_ > max_ref:
            logger.debug(f'{varn} ({unit}): {min_:.6f} < {min_ref} or {max_:.6f} > {max_ref}')
            return ' '.join([
                f'(min, max) of variable outside warning range: ({min_ref},',
                f'{max_ref})'])
        return True
    else:
        min_ref = ERROR_RANGE[varn][0]
        max_ref = ERROR_RANGE[varn][1]
        if min_ < min_ref or max_ > max_ref:
            logger.error(f'{varn} ({unit}): {min_:.4f} < {min_ref} or {max_:.4f} > {max_ref}')
            return False
        return True


def check_unit(da, unit_ref):
    unit = da.attrs['units']
    if unit != unit_ref:
        logger.error(f'{unit} != {unit_ref}')
        return False
    return True


def check_pr(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'pr'):
        return False
    return check_range_full(da, 'pr', warning=True)


def check_psl(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'Pa'):
        return False
    if not check_range_full(da, 'psl'):
        return False
    return check_range_full(da, 'psl', warning=True)


def check_ps(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'Pa'):
        return False
    if not check_range_full(da, 'ps'):
        return False
    return check_range_full(da, 'ps', warning=True)


def check_siconc(da, model):
    # see check_tos
    #if model == 'ICON-ESM-LR':
    #    if da.dims != DIMENSIONS_3D_3:
    #        logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
    #        return False
    #else:
    #   if da.dims != DIMENSIONS_3D_2:
    #       raise VariableChecksError(f'{da.dims} != {DIMENSIONS_3D_2}')
    if not check_unit(da, '%'):
        return False
    if not check_range_full(da, 'siconc'):
        return False
    return check_range_full(da, 'siconc', warning=True)


def check_tas(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tas'):
        return False
    return check_range_full(da, 'tas', warning=True)


def check_tasmin(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tasmin'):
        return False
    return check_range_full(da, 'tasmin', warning=True)


def check_tasmax(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tasmax'):
        return False
    return check_range_full(da, 'tasmax', warning=True)


def check_tos(da, model):
    # NOTE: tos has several different dimension names, e.g., (lat, lon),
    # (x, y), (j, i), (nj, ni), ... At the moment they are left as they are and the
    # dimension name check is disabled.
    #if model == 'ICON-ESM-LR':
    #    if da.dims != DIMENSIONS_3D_3:
    #        logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
    #        return False
    #else:
    #   if da.dims != DIMENSIONS_3D_2:
    #       raise VariableChecksError(f'{da.dims} != {DIMENSIONS_3D_2}')
    if not check_unit(da, 'degC'):
        return False
    if not check_range_full(da, 'tos'):
        return False
    return check_range_full(da, 'tos', warning=True)


def check_clt(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, '%'):
        return False
    if da.max() <= 5.:
        logger.error('clt (%) max < 5. (fraction instead of % ?)')
        return False
    if not check_range_full(da, 'clt'):
        return False
    return check_range_full(da, 'clt', warning=True)


def check_hurs(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, '%'):
        return False
    if da.max() <= 5.:
        logger.error('hurs (%) max < 5. (fraction instead of % ?)')
        return False
    if not check_range_full(da, 'hurs'):
        return False
    return check_range_full(da, 'hurs', warning=True)


def check_huss(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, '1'):
        return False
    if not check_range_full(da, 'huss'):
        return False
    return check_range_full(da, 'huss', warning=True)


def check_rlut(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rlut'):
        return False
    return check_range_full(da, 'rlut', warning=True)


def check_rsdt(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsdt'):
        return False
    return check_range_full(da, 'rsdt', warning=True)


def check_rsut(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsut'):
        return False
    return check_range_full(da, 'rsut', warning=True)


def check_rtmt(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rtmt'):
        return False
    return check_range_full(da, 'rtmt', warning=True)


def check_rlds(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rlds'):
        return False
    return check_range_full(da, 'rlds', warning=True)


def check_rlus(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rlus'):
        return False
    return check_range_full(da, 'rlus', warning=True)


def check_rsds(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsds'):
        return False
    return check_range_full(da, 'rsds', warning=True)


def check_rsus(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsus'):
        return False
    return check_range_full(da, 'rsus', warning=True)


def check_hfss(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'hfss'):
        return False
    return check_range_full(da, 'hfss', warning=True)


def check_hfls(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'hfls'):
        return False
    return check_range_full(da, 'hfls', warning=True)


def check_prw(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2'):
        return False
    if not check_range_full(da, 'prw'):
        return False
    return check_range_full(da, 'prw', warning=True)


def check_tau(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'Pa'):
        return False
    if not check_range_full(da, 'tauu'):
        return False
    return check_range_full(da, 'tauu', warning=True)


def check_sftlf(da, model):
    if da.dims != DIMENSIONS_2D:
        logger.error(f'{da.dims} != {DIMENSIONS_2D}')
        return False
    if not check_unit(da, '%'):
        return False
    if not check_range_full(da, 'sftlf'):
        return False
    return check_range_full(da, 'sftlf', warning=True)


def check_areacella(da, model):
    if da.dims != DIMENSIONS_2D:
        logger.error(f'{da.dims} != {DIMENSIONS_2D}')
        return False
    if not check_unit(da, 'm2'):
        return False
    if not check_range_full(da, 'areacella'):
        return False
    return check_range_full(da, 'areacella', warning=True)


def check_evspsbl(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'evspsbl'):
        return False
    return check_range_full(da, 'evspsbl', warning=True)


def check_zg500(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'm'):
        return False
    if not check_range_full(da, 'zg500'):
        return False
    return check_range_full(da, 'zg500', warning=True)


def check_gpp(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'gpp'):
        return False
    return check_range_full(da, 'gpp', warning=True)


def check_ra(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'ra'):
        return False
    return check_range_full(da, 'ra', warning=True)


def check_co2mass(da, model):
    if da.dims != DIMENSIONS_1D:
        logger.error(f'{da.dims} != {DIMENSIONS_1D}')
        return False
    if not check_unit(da, 'kg'):
        return False
    if not check_range_full(da, 'co2mass'):
        return False
    return check_range_full(da, 'co2mass', warning=True)


def check_mrso(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2'):
        return False
    if not check_range_full(da, 'mrso'):
        return False
    return check_range_full(da, 'mrso', warning=True)


def check_lai(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, '1'):
        return False
    if not check_range_full(da, 'lai'):
        return False
    return check_range_full(da, 'lai', warning=True)


def check_mrro(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'mrro'):
        return False
    return check_range_full(da, 'mrro', warning=True)


def check_tsl(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_4D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_4D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_4D:
            logger.error(f'{da.dims} != {DIMENSIONS_4D}')
            return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tsl'):
        return False
    return check_range_full(da, 'tsl', warning=True)


def check_mrsol(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_4D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_4D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_4D:
            logger.error(f'{da.dims} != {DIMENSIONS_4D}')
            return False
    if not check_unit(da, 'kg m-2'):
        return False
    if not check_range_full(da, 'mrsol'):
        return False
    return check_range_full(da, 'mrsol', warning=True)


def check_ta(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_4D_4:
            logger.error(f'{da.dims} != {DIMENSIONS_4D_4}')
            return False
    else:
        if da.dims != DIMENSIONS_4D_2:
            logger.error(f'{da.dims} != {DIMENSIONS_4D_2}')
            return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'ta'):
        return False
    return check_range_full(da, 'ta', warning=True)


def check_treeFrac(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, '%'):
        return False
    if da.max() <= 5.:
        logger.error('clt (%) max < 5. (fraction instead of % ?)')
        return False
    if not check_range_full(da, 'treeFrac'):
        return False
    return check_range_full(da, 'treeFrac', warning=True)


def check_hfds(da, model):
    #if model == 'ICON-ESM-LR':
    #    if da.dims != DIMENSIONS_3D_3:
    #        logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
    #        return False
    #else:
    #    if da.dims != DIMENSIONS_3D:
    #        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
    #        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'hfds'):
        return False
    return check_range_full(da, 'hfds', warning=True)


def check_zos(da, model):
    #if model == 'ICON-ESM-LR':
    #    if da.dims != DIMENSIONS_3D_3:
    #        logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
    #        return False
    #else:
    #    if da.dims != DIMENSIONS_3D:
    #        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
    #        return False
    if not check_unit(da, 'm'):
        return False
    if not check_range_full(da, 'zos'):
        return False
    return check_range_full(da, 'zos', warning=True)


def check_uas(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'm s-1'):
        return False
    if not check_range_full(da, 'uas'):
        return False
    return check_range_full(da, 'uas', warning=True)


def check_vas(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'm s-1'):
        return False
    if not check_range_full(da, 'vas'):
        return False
    return check_range_full(da, 'vas', warning=True)


def check_sfcWind(da, model):
    if model == 'ICON-ESM-LR':
        if da.dims != DIMENSIONS_3D_3:
            logger.error(f'{da.dims} != {DIMENSIONS_3D_3}')
            return False
    else:
        if da.dims != DIMENSIONS_3D:
            logger.error(f'{da.dims} != {DIMENSIONS_3D}')
            return False
    if not check_unit(da, 'm s-1'):
        return False
    if not check_range_full(da, 'sfcWind'):
        return False
    return check_range_full(da, 'sfcWind', warning=True)


def variable_checks(ds, varn, model):
    if varn in ['pr']:
        return check_pr(ds[varn], model)
    elif varn in ['psl']:
        return check_psl(ds[varn], model)
    elif varn in ['ps']:
        return check_ps(ds[varn], model)
    elif varn in ['siconc', 'siconca']:
        return check_siconc(ds[varn], model)
    elif varn in ['tas']:
        return check_tas(ds[varn], model)
    elif varn in ['tasmax']:
        return check_tasmax(ds[varn], model)
    elif varn in ['tasmin']:
        return check_tasmin(ds[varn], model)
    elif varn in ['ta']:
        return check_ta(ds[varn], model)
    elif varn in ['tos']:
        return check_tos(ds[varn], model)
    elif varn in ['tsl']:
        return check_tsl(ds[varn], model)
    elif varn in ['clt']:
        return check_clt(ds[varn], model)
    elif varn in ['hurs']:
        return check_hurs(ds[varn], model)
    elif varn in ['huss']:
        return check_huss(ds[varn], model)
    elif varn in ['rlut', 'rlutcs']:
        return check_rlut(ds[varn], model)
    elif varn in ['rsdt']:
        return check_rsdt(ds[varn], model)
    elif varn in ['rsut', 'rsutcs']:
        return check_rsut(ds[varn], model)
    elif varn in ['rsds', 'rsdscs']:
        return check_rsds(ds[varn], model)
    elif varn in ['rsus', 'rsuscs']:
        return check_rsus(ds[varn], model)
    elif varn in ['rlds', 'rldscs']:
        return check_rlds(ds[varn], model)
    elif varn in ['rlus']:
        return check_rlus(ds[varn], model)
    elif varn in ['rtmt']:
        return check_rtmt(ds[varn], model)
    elif varn in ['hfls']:
        return check_hfls(ds[varn], model)
    elif varn in ['hfss']:
        return check_hfss(ds[varn], model)
    elif varn in ['hfds']:
        return check_hfds(ds[varn], model)
    elif varn in ['prw']:
        return check_prw(ds[varn], model)
    elif varn in ['tauu', 'tauv']:
        return check_tau(ds[varn], model)
    elif varn in ['sftlf']:
        return check_sftlf(ds[varn], model)
    elif varn in ['areacella']:
        return check_areacella(ds[varn], model)
    elif varn in ['evspsbl', 'tran', 'evspsblveg', 'evspsblsoi']:
        return check_evspsbl(ds[varn], model)
    elif varn in ['zg500']:
        return check_zg500(ds[varn], model)
    elif varn in ['gpp', 'npp', 'nbp']:
        return check_gpp(ds[varn], model)
    elif varn in ['ra', 'rh']:
        return check_ra(ds[varn], model)
    elif varn in ['co2mass']:
        return check_co2mass(ds[varn], model)
    elif varn in ['lai']:
        return check_lai(ds[varn], model)
    elif varn in ['mrso', 'mrsos']:
        return check_mrso(ds[varn], model)
    elif varn in ['mrro', 'mrros']:
        return check_mrro(ds[varn], model)
    elif varn in ['mrsol']:
        return check_mrsol(ds[varn], model)
    elif varn in ['x',
                  'y']:
        pass  # TODO
    elif varn in ['treeFrac']:
        return check_treeFrac(ds[varn], model)
    elif varn in ['zos']:
        return check_zos(ds[varn], model)
    elif varn in ['uas']:
        return check_uas(ds[varn], model)
    elif varn in ['vas']:
        return check_vas(ds[varn], model)
    elif varn in ['sfcWind']:
        return check_sfcWind(ds[varn], model)
    else:
        raise NotImplementedError(f'{varn}')
