#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2020-12-04 16:00:38 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

TODO:
- add option to check if original files have been updated and need recalculation
  -> this could use Urs' dot-files and check if they are older in the raw folder
  than in the ng folder

"""
import argparse
import datetime
import gc
import hashlib
import logging
import os
import subprocess
import traceback

import numpy as np
import xarray

from checks.basic_checks import basic_checks, check_time_decoding
from checks.variable_checks import variable_checks
from fixes import fixes
from utils import string_functions as sf

xarray.set_options(file_cache_maxsize=1, keep_attrs=True)

logger = logging.getLogger(__name__)

LOG_PATH = './logfiles'
SAVE_PATH_TMP = '/net/co2/c2sm-data/rlorenz/tmp/cmip6ng_debug/'


def read_args():
    """Parse arguments form the command line."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        '--overwrite', '-o', dest='overwrite', default='false',
        choices=['all', 'update', 'false'],
        help='all: overwrite all files; update: overwrite outdated files')
    parser.add_argument(
        '--log-level', dest='log_level', default=20,
        type=int, choices=[10, 20, 30, 40],
        help='Set logging level (10: debug, 20: info, 30: warning, 40: error')
    parser.add_argument(
        '--log-file', dest='log_file', default=None,
        type=str, help='logfile name (use "STDOUT" to log to console)')
    parser.add_argument(
        '--debug', dest='debug', action='store_true',
        help=f'Run in debug mode (output is saved to: {SAVE_PATH_TMP})')

    args = parser.parse_args()

    if args.log_file is None:
        args.log_file = datetime.datetime.today().strftime('%Y%m%d_%H%M%S.log')
        if args.debug:
            args.log_file = f'debug_{args.log_file}'
    if args.log_file.lower() == 'stdout':
        args.log_file = None
    else:
        # make sure the logfile is in the LOG_PATH directory
        args.log_file = os.path.join(LOG_PATH, os.path.basename(args.log_file))

    return args


def get_git_info():
    """
    Get information about the Git repository.

    Returns
    -------
    git_info : string
        Information about the git repository:
        time_stamp git@url:git_rep.git: branch_name ##

    Information
    -----------
    time_stamp :  The current time
    git@url:git_rep.git : Origin of the git repository
        git config --get remote.origin.url
    branch_name : currently checked out branch
        git rev-parse --abbrev-ref HEAD
    ## : revision hash or revision tag
        git describe --always
    """
    status = str(subprocess.check_output(['/usr/bin/git', 'status'])).split('\\n')[-2]
    if not (status == 'nothing to commit, working tree clean' or
            status == 'nothing added to commit but untracked files present (use "git add" to track)'):
        errmsg = ' '.join([
            'Git status not clean! To fix this revert or commit & push all',
            'local changes'])
        logger.error(errmsg)
        raise ValueError(errmsg)

    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    revision = subprocess.check_output([
        '/usr/bin/git', 'describe', '--always']).strip().decode()
    rep_name = subprocess.check_output([
        '/usr/bin/git', 'config', '--get', 'remote.origin.url']).strip().decode()
    branch_name = subprocess.check_output([
        '/usr/bin/git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip().decode()
    return f'{time} {rep_name}: {branch_name} {revision}'


def add_attribute(ds, files_org, hashes_org, dd={}, key='cmip6-ng'):
    """
    Add information to netCDF attributes.

    Parameters
    ----------
    ds : xarray.Dataset
    files_org : list
    hashes_org : list
    dd : dict, optional
        Dictionary with information (will display as key = dd[key])
    key : string, optional
        Attribute name

    Returns
    -------
    ds : same as input
    """
    dd['contact'] = 'cmip6-archive@env.ethz.ch'
    dd['description'] = 'ETH Zurich CMIP6 "next generation" (ng) archive.'
    dd['disclaimer'] = ' '.join([
        'This dataset is provided "as is", without warranty of any kind.'])
    dd['ownership'] = ' '.join([
        'The ownership of this dataset remains with the original provider'])
    ds.attrs[key] = '\n'.join([''] + [f'{k} = {dd[k]}' for k in sorted(dd.keys())])

    ds.attrs['original_file_names'] = ', '.join(files_org)
    ds.attrs['original_file_hash_codes'] = ', '.join(hashes_org)


def add_flag(ds, flag=0):
    ds['file_qf'] = xarray.DataArray(
        data=np.array(flag, dtype=float),
        attrs={
            'long_name': 'File Quality Flag',
            'description': ' '.join([
                'File quality flag added by the cmip6-ng processor. Flags',
                'may be added together if more than one applies at the same',
                'time (e.g., 11: minor fixes in file and warning range',
                'exceeded.']),
            '_FillValue': -1.,
            'missing_value': -1.,
            'valid_range': [0, 111],
            'flag_values': [0, 1, 10, 100],
            'flag_meanings': ' '.join([
                'no_issues',
                'minor_fixes',
                'warning_range_exceeded',
                'unfixed_issues',
            ])})


def get_hashes(files):
    """Retrieve the hashes for a list of files"""
    hashes = []
    for file_ in files:
        with open(file_, 'rb') as f:
            hash_ = hashlib.sha256(f.read()).hexdigest()
            hashes.append(hash_)
    return hashes


def preprocess(ds):
    path = ds.encoding['source']

    if 'GFDL-CM4' in path and 'historical' in path:
        ds = ds.drop('height', errors='ignore')
    if 'EC-Earth3' in path:
        ds.lat.values = np.around(ds['lat'].values, 2)
    return ds


def process_native_grid(
        files, time_res, git_info='',
        overwrite='update', skip_paths=[], debug=False):
    """
    Main function for fixes and checks.

    Build the target folder and filename strings. Create the folder and check
    if the file exists. Apply fixes to know issues and check for remaining
    problems.

    Parameters
    ----------
    files : list of strings
        A list of filenames which will be concatenated along the time axis.
    time_res : {'ann', 'mon', 'day'}
        The time resolution of the target file.
    overwrite : {'all', 'update', 'false'}, optional
        * all : overwrite all files
        * update : only overwrite files which have been update from upstream
        * false : don't overwrite (only save not yet existing files)
    skip_paths : list of strings
        A list of paths which is skipped (e.g., because not fixable errors have
        been encountered in earlier runs)
    Returns
    -------
    save_filename : string or None
        The filename of the saved file or None if an issue is found or an
        error is encountered.
    """
    if os.path.dirname(files[0]) in skip_paths:
        logger.debug(f'Skip source path:\n  {os.path.dirname(files[0])}')
        return None, False

    # extract metadata from path and filename (and check if they match)
    metadata, (starttime, endtime) = sf.check_filename_vs_path(
        files, 6 if time_res == 'fx' else 7)

    # build path and filename for ng files
    save_path = sf.get_ng_path(
        varn=metadata['varn'],
        time_res=time_res,
    )
    if debug:
        save_path = SAVE_PATH_TMP
    os.makedirs(save_path, exist_ok=True)

    save_filename = sf.get_ng_filename(
        varn=metadata['varn'],
        time_res=time_res,
        model=metadata['model'],
        run_type=metadata['run_type'],
        ensemble=metadata['ensemble'],
    )
    save_filename = os.path.join(save_path, save_filename)

    hashes = get_hashes(files)

    # skip existing files or start processing
    if os.path.isfile(save_filename) and overwrite == 'false':
        logger.debug(f'Skip existing file:\n  {save_filename}')
        return save_filename, False
    elif os.path.isfile(save_filename) and overwrite == 'update':
        ds_ng = xarray.open_dataset(save_filename, decode_cf=False)
        # if the ng file does not have hashes yet update in any case...
        if 'original_file_hash_codes' in ds_ng.attrs.keys():
            hashes_ng = ds_ng.attrs['original_file_hash_codes']
            # ...otherwise only update if hashes dont' fit
            if ', '.join(hashes) == hashes_ng:
                logger.debug(f'Skip identical hash file:\n  {save_filename}')
                return save_filename, False
        logger.info(f'Update target file:\n  {save_filename}')
    else:
        logger.info(f'Process target file:\n  {save_filename}')

    try:
        ds = xarray.open_mfdataset(
            files,
            use_cftime=True,
            concat_dim='time',
            combine='by_coords',
            coords='minimal',
            data_vars='minimal',
            compat='override',
            preprocess=preprocess,
        )
        pass_ = check_time_decoding(ds, time_res, starttime, endtime)
        if not pass_:
            # NOTE: there are many files with non-full coverage per year that do not
            # pass this test, so logging this is very verbose
            logger.debug('Inconsistent time axis after combining/decoding time!')
            return None, False
    except Exception:
        gc.collect()
        logger.error('Encountered exception in xarray.open_mfdataset()! Skipping model.')
        logger.error(f'First source file:\n  {files[0]}')
        logger.error(traceback.format_exc())
        return None, False

    ds, unfixed_issues, fixes_str, flag = fixes.apply_fixes(ds, metadata, time_res)
    assert flag in [0, 1, 100]

    # basic checks
    try:
        if not basic_checks(ds.copy(), metadata, time_res):
            logger.error(f'First source file:\n  {files[0]}')
            return None, False
    except Exception:
        logger.error('Encountered exception in basic_checks()! Skipping model.')
        logger.error(f'First source file:\n  {files[0]}')
        logger.error(traceback.format_exc())
        return None, False

    # variable depended checks
    try:
        ui = variable_checks(ds.copy(), metadata['varn'], metadata['model'])
    except Exception:
        logger.error('Encountered exception in variable_checks()! Skipping model.')
        logger.error(f'First source file:\n  {files[0]}')
        logger.error(traceback.format_exc())
        return None, False
    if isinstance(ui, bool) and ui:  # all okay
        pass
    elif isinstance(ui, bool) and not ui:  # exceeded error range
        logger.error(f'First source file:\n  {files[0]}')
        return None, False
    elif isinstance(ui, str):  # exceeded warning range
        unfixed_issues.append(ui)  # add to issues list
        flag += 10
    else:  # DEBUG: this should not happen!
        logger.error('Unexpected return value iu: {iu}')
        logger.error(f'First source file:\n  {files[0]}')
        raise ValueError

    encoding = {}
    # force all historical and SSP files to have the same time units
    if time_res != 'fx' and metadata['run_type'] in [
            'historical', 'ssp119', 'ssp126', 'ssp245',
            'ssp370', 'ssp434', 'ssp460', 'ssp534-over', 'ssp585']:
        encoding['time'] = {'units': 'days since 1850-01-01'}
        fixes_str += ', re-define time unit'

    # saving sometimes fails due to multiple fill values which I assume is a
    # bug in xarray. This is a workaround.
    if '_FillValue' in ds[metadata['varn']].encoding.keys():
        fill_value = ds[metadata['varn']].encoding['_FillValue']
        encoding[metadata['varn']] = {'_FillValue': fill_value}
    unfixed_issue_str = '; '.join([i for i in unfixed_issues if i is not None])

    add_attribute(ds, files, hashes, {
        'unfixed_issues': unfixed_issue_str,
        'fixes': fixes_str,
        'git': git_info})
    add_flag(ds, flag)

    ds.to_netcdf(save_filename, format='NETCDF4_CLASSIC', encoding=encoding)
    ds.close()

    logger.debug(f'Saved file:\n  {save_filename}')
    return save_filename, True
