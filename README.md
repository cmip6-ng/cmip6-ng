# cmip6-ng

A collection of scripts for checks, fixes, and general post-processing for CMIP6 models. Main aim is to create a "cmip6-ng" archive similar to the existing cmip5-ng archive.